import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router';
import LogBook from '../views/LogBook.vue';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'log-book',
    component: LogBook,
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
