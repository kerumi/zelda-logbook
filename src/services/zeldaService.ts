import axios from 'axios';

interface Result {
  id: string;
  name: string;
  description: string;
}

const baseUrl = 'https://zelda.fanapis.com/api';

const apiBaseUrls: Record<string, string> = {
  Game: `${baseUrl}/games`,
  Characters: `${baseUrl}/characters`,
  Monsters: `${baseUrl}/monsters`,
  Items: `${baseUrl}/items`,
};

const fetchResults = async (
  selectedItem: string,
  searchQuery: string,
): Promise<Result[]> => {
  const apiUrl = apiBaseUrls[selectedItem] || apiBaseUrls.Items;
  const params: { name?: string } = {};

  if (searchQuery.trim()) {
    params.name = searchQuery.trim();
  }

  try {
    const response = await axios.get(apiUrl, { params });
    return response.data.data.map((item: Result) => ({
      id: item.id,
      name: item.name,
      description: item.description,
    }));
  } catch (error) {
    console.error('Error fetching results:', error);
    return [];
  }
};

export default {
  fetchResults,
};
