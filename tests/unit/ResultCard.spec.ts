import { mount } from '@vue/test-utils';
import ResultCard from '@/components/ResultCard.vue';

describe('ResultCard.vue', () => {
  const props = {
    id: '1',
    name: 'Test Name',
    description: 'Test Description',
  };

  it('renders correctly with given props', () => {
    const wrapper = mount(ResultCard, {
      props,
    });

    expect(wrapper.find('h3').text()).toBe(props.name);
    expect(wrapper.find('.description').text()).toBe(props.description);
  });

  it('renders an image with correct src and alt attributes', () => {
    const wrapper = mount(ResultCard, {
      props,
    });

    const img = wrapper.find('img');
    expect(img.exists()).toBe(true);
    expect(img.attributes('alt')).toBe('Icon');
  });

  it('has the correct styles applied', () => {
    const wrapper = mount(ResultCard, {
      props,
    });

    const card = wrapper.find('.result-card');
    expect(card.exists()).toBe(true);
    expect(card.classes()).toContain('result-card');
  });
});
