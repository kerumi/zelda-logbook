import { mount } from '@vue/test-utils';
import NavBar from '@/components/NavBar.vue';

describe('NavBar.vue', () => {
  const navItems = ['Home', 'About', 'Contact'];

  it('renders the nav bar', () => {
    const wrapper = mount(NavBar, {
      props: { navItems, selectedItem: 'Home', onSelectItem: jest.fn() },
    });

    expect(wrapper.find('nav').exists()).toBe(true);
    expect(wrapper.findAll('.nav-item').length).toBe(navItems.length);
  });

  it('highlights the selected item', () => {
    const selectedItem = 'About';
    const wrapper = mount(NavBar, {
      props: { navItems, selectedItem, onSelectItem: jest.fn() },
    });

    const activeItem = wrapper.find('.nav-item.active');
    expect(activeItem.text()).toBe(selectedItem);
  });

  it('calls onSelectItem when an item is clicked', async () => {
    const onSelectItem = jest.fn();
    const wrapper = mount(NavBar, {
      props: { navItems, selectedItem: 'Home', onSelectItem },
    });

    await wrapper.findAll('.nav-item')[1].trigger('click');
    expect(onSelectItem).toHaveBeenCalledWith('About');
  });

  it('updates the selected item when props change', async () => {
    const wrapper = mount(NavBar, {
      props: { navItems, selectedItem: 'Home', onSelectItem: jest.fn() },
    });

    await wrapper.setProps({ selectedItem: 'Contact' });
    const activeItem = wrapper.find('.nav-item.active');
    expect(activeItem.text()).toBe('Contact');
  });

  it('supports keyboard interaction', async () => {
    const onSelectItem = jest.fn();
    const wrapper = mount(NavBar, {
      props: { navItems, selectedItem: 'Home', onSelectItem },
    });

    const secondNavItem = wrapper.findAll('.nav-item')[1];
    await secondNavItem.trigger('keypress');
    expect(onSelectItem).toHaveBeenCalledWith('About');
  });
});
