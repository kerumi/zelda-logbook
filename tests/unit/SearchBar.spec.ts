import { mount } from '@vue/test-utils';
import SearchBar from '@/components/SearchBar.vue';
import debounce from 'lodash.debounce';

jest.mock('lodash.debounce', () => jest.fn((fn) => fn));

describe('SearchBar.vue', () => {
  const initialQuery = 'Initial Query';

  it('renders correctly with given initialQuery', () => {
    const wrapper = mount(SearchBar, {
      props: { initialQuery },
    });

    const input = wrapper.find('input');
    expect(input.element.value).toBe(initialQuery);
  });

  it('emits update:query and search events when submit form', async () => {
    const wrapper = mount(SearchBar, {
      props: { initialQuery: '' },
    });

    const newQuery = 'New Search Query';

    const input = wrapper.find('input');
    await input.setValue(newQuery);
    await wrapper.find('form').trigger('submit.prevent');

    expect(wrapper.emitted('update:query')).toBeTruthy();
    expect(wrapper.emitted('update:query')?.[0]).toEqual([newQuery]);

    expect(wrapper.emitted('search')).toBeTruthy();
    expect(wrapper.emitted('search')?.[0]).toEqual([newQuery]);
  });

  it('updates the input value on v-model update', async () => {
    const wrapper = mount(SearchBar, {
      props: { initialQuery: '' },
    });

    const input = wrapper.find('input');
    const newValue = 'Updated Query';
    await input.setValue(newValue);

    expect(input.element.value).toBe(newValue);
  });

  it('debounces search event', async () => {
    const wrapper = mount(SearchBar, {
      props: { initialQuery: '' },
    });

    const newQuery = 'Debounced Query';

    const input = wrapper.find('input');
    await input.setValue(newQuery);

    expect(debounce).toHaveBeenCalled();
  });

  it('renders search icon with correct attributes', () => {
    const wrapper = mount(SearchBar, {
      props: { initialQuery: '' },
    });

    const img = wrapper.find('.search-icon img');
    expect(img.exists()).toBe(true);
    expect(img.attributes('alt')).toBe('Search Icon');
  });

  it('triggers search on search icon click', async () => {
    const wrapper = mount(SearchBar, {
      props: { initialQuery: '' },
    });

    await wrapper.find('.search-icon').trigger('click');

    expect(wrapper.emitted('search')).toBeTruthy();
  });

  it('triggers search on Enter key press', async () => {
    const wrapper = mount(SearchBar, {
      props: { initialQuery: '' },
    });

    await wrapper.find('.search-icon').trigger('keypress.enter');

    expect(wrapper.emitted('search')).toBeTruthy();
  });
});
