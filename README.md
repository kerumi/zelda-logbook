# Zelda Logbook

Zelda LogBook is a web application that allows you to get information about The Legend of Zelda games.

### [Demo is available here](https://zelda-logbook.vercel.app/#/)

## Project setup

```
yarn install
```

### Compiling and Hot-reloading for Development

```
yarn serve
```

### Compiles and minifies for production

```
yarn build
```

### Run your unit tests

```
yarn test:unit
```

### Lints and fixes files

```
yarn lint
```
